<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeExpense extends Model
{
    protected $fillable = [
       'employee_salary',
       'goods',
       'qty',
       'qty_amount',
       'maintenance',
       'maintenance_amount',
       'others',
       'description',
       'other_amount',
       'grand_total',
    ];
}
