<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allotment extends Model
{
  protected $fillable =[
      'fullname', 
      'number',
      'cnic',
      'plotno',
      'block'
  ];
}
