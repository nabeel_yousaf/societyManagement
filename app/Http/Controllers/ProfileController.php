<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Hash;

class ProfileController extends Controller
{
   public function profile(){
       return view('layouts.Adminprofile.profile');
   }
   public function profileUpdate(Request $request){

       $request->validate([
          'name' => 'required|string',
          'email' => 'required|email',
         //  'phone' => 'required',
       ]);
       $user = Auth::user();
       $user->name = $request['name'];
       $user->email = $request['email'];
      //  $user->phone = $request['phone'];
       $user->save();
         return redirect('/profile')->with('success', 'Profile Updated Successfully');
   }
   public function changePasswordForm(){
      return view('layouts.Adminprofile.changePasswordForm');
   }
   public function updatePassword(Request $request){
        if(!(Hash::check($request->get('current_password'), Auth::user()->password))){
           return redirect('/changePassword')->with('error', "Your Current Password does not match");
        }
        if(strcmp($request->get('current_password'),$request->get('new_password'))==0){
                   return redirect('/changePassword')->with('error', "Your Current Password will not same with the new password");
        }
      $validatedData =  $request->validate([
           'current_password'=> 'required',
           'new_password' => 'required|min:8|confirmed',
        ]);
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return redirect('/changePassword')->with('success', 'Password changed successfully');
   }
}
