<?php

namespace App\Http\Controllers;

use App\Allotment;
use Illuminate\Http\Request;

class AllotmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allotments = Allotment::latest()->paginate(5);
      return view('layouts.Allotment.Show',['allotments' => $allotments])
       ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Allotment.Add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $validateData = $request->validate([
            'fullname' => 'required',
            'number' => 'required|numeric',
            'cnic' => 'required',
            'plotno' => 'required',
            'block' => 'required',
        ]);   
          $allotments = new Allotment();
         $allotments->fullname = $request->input('fullname');
        $allotments->number = $request->input('number');
        $allotments->cnic = $request->input('cnic');
        $allotments->plotno = $request->input('plotno');
        $allotments->block = $request->input('block');
          $allotments->save();
        return redirect('/allotments')->with('success', 'Successfully Added', $allotments);
 
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function show(Allotment $allotment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $allotments = Allotment::findOrFail($id);
              return view('layouts.Allotment.Editallotment', compact('allotments'));
             
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Allotment $allotment)
    {
          $validateData = $request->validate([
            'fullname' => 'required',
            'number' => 'required|numeric',
            'cnic' => 'required',
            'plotno' => 'required',
            'block' => 'required',
        ]);
          $allotments = new Allotment();
         $allotments->fullname = $request->input('fullname');
        $allotments->number = $request->input('number');
        $allotments->cnic = $request->input('cnic');
        $allotments->plotno = $request->input('plotno');
        $allotments->block = $request->input('block');
          $allotments->save();
        return redirect('/allotments')->with('success', 'Successfully Added', $allotments);
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $allotments = Allotment::find($id);
        $allotments->delete();
        return redirect('/allotments')->with('danger', 'Successfully  Deleted');
 
    }
}
