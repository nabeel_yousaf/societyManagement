<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use Redirect;
class MemberController extends Controller
{
    // public function __construct(){
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $total = Member::count();
        $members = Member::orderBy('id', 'DESC')->paginate(10);
      return view('Memberview.index',['members' => $members])
       ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Memberview.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validateData = $request->validate([
            'fullname' => 'required',
            'number' => 'required|numeric',
            'email' => 'required',
            'address' => 'required',
            'cnic' => 'required',
            'designation' => 'required',
            'image'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $members = new Member();
         $members->fullname = $request->input('fullname');
        $members->number = $request->input('number');
        $members->email = $request->input('email');
        $members->address = $request->input('address');
        $members->cnic = $request->input('cnic');
        $members->designation = $request->input('designation');
       if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalName();
            $filename = time(). '.' .$extension;
            $file->move('picture/', $filename);
          $members->image = $filename;
        } else{
            return $request;
            $members->image = '';

        }
      $members->save();
        return Redirect::to('/members')->with('success', 'Member Created Successfully', $members);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $members = Member::findOrFail($id);
              return view('Memberview.update', compact('members'));
             
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validateData = $request->validate([
            'fullname' => 'required',
            'number' => 'required|numeric',
            'email' => 'required',
            'address' => 'required',
            'cnic' => 'required',
            'designation' => 'required',
            'image'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
         $member = new Member();
         $member->fullname = $request->input('fullname');
        $member->number = $request->input('number');
        $member->email = $request->input('email');
        $member->address = $request->input('address');
        $member->cnic = $request->input('cnic');
        $member->designation = $request->input('designation');

        // if($request->file != ''){
        //     $path = public_path().'/picture';

        //     //code for remove old file
        //     if($member->file != '' && $member->file != null){
        //         $file_old = $path.$member->file;
        //         unlink($file_old);
        //     }
        //     // upload file
        //     $file = $request->file;
        //     $filename = $file->getClientOriginalName();
        //     $file->move($path,$filename);
            if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalName();
            $filename = time(). '.' .$extension;
            $file->move('picture/', $filename);
          $member->image = $filename;
        } else{
            return $request;
            $member->image = '';

        }
          $member->save();
        return Redirect::to('/members')->with('success', 'Member Updated Successfully', $member);
 
        }
      
       
       

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $members = Member::find($id);
        $members->delete();
        return redirect('/members')->with('success', 'Member Deleted Successfully');
    }
}
