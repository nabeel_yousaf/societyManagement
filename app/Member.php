<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable =[
        'fullname',
        'number',
        'email',
        'address',
        'cnic',
        'designation',
        'image'
    ];
}
