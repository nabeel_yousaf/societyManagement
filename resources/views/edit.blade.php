@extends('layouts.master')

@section('title')

 Society Dashboard
@endsection



@section('content')
  <div class="container-fluid">  
        <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Update Member</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                <li class="breadcrumb-item active">Update Member</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      
  <section class="content">
     <div class="container-fluid">
     <form action="{{route('members.update', $member->id)}}" method="post" enctype="multipart/form-data">
          @csrf
           @method('PATCH')
               <div class="row">
    <div class="col">
        <label for="name">Full Name</label>
    <input type="text" name="fullname" value="{{$member->fullname}}" class="form-control" placeholder="Full name">
    <span class="text-danger">{{$errors->first('fullname')}}</span>
    </div>
    <div class="col">
         <label for="number">Number</label>
      <input type="text" name="number" value="{{$member->number}}" class="form-control" placeholder="Contact">
    <span class="text-danger">{{$errors->first('number')}}</span>
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" name="email" value="{{$member->email}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    <span class="text-danger">{{$errors->first('email')}}</span>
   
  </div>
  <div class="form-group">
    <label for="address">Address</label>
    <input type="text" class="form-control" value="{{$member->address}}" name="address" id="address" placeholder="Address">
    <span class="text-danger">{{$errors->first('address')}}</span>
 
  </div>
             <div class="row">
    <div class="col">
        <label for="cnic">Cnic</label>
      <input type="text" name="cnic" value="{{$member->cnic}}" class="form-control" placeholder="Cnic Number">
    <span class="text-danger">{{$errors->first('cnic')}}</span>
   
    </div>
    <div class="col">
         <label for="designation">Designation</label>
       <select class="form-control" name="designation"  >
       <option value="{{$member->designation}}">{{$member->designation}}</option>
           <option value="CEO">CEO</option>
       <option value="Accounts">Accounts</option>
       <option value="Secretary">Secretary</option>
        <option value="Operator Manager">Operator Manger</option>
   
         </select>
     <span class="text-danger">{{$errors->first('designation')}}</span>

    </div>
  </div>
  <div class="form-group">
    <label for="image" class="mt-3">Choose Image</label><br>
  <input type="file" id="images" class="custom-file-input"  name="image">
  <img src="{{asset('picture/' .$member->image)}}" value="{{!! $member->image !!}}" alt="image" class="img-fluid img-thumbnails" width="75" />

    <span class="text-danger">{{$errors->first('image')}}</span>
  </div>

  <button type="submit" class="btn btn-primary mt-3">Update</button>
</form>
         </div>  
      </section>
@endsection

@section('scripts')
 <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
@endsection