@extends('layouts.master')

@section('title')

 Society Dashboard
@endsection

@section('content')
  <div class="container-fluid">
    
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Add Allotment</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                <li class="breadcrumb-item active">Add Allotment</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
 <section class="content">
        <div class="container-fluid">
        <form method="POST" action="{{route('allotments.store')}}" enctype="multipart/form-data">
           @csrf
               
    <div class="col">
        <label for="name">Full Name</label>
      <input type="text" name="fullname" class="form-control" placeholder="Full name">
    <span class="text-danger">{{$errors->first('fullname')}}</span>
    </div>
    <div class="col">
         <label for="number">Number</label>
      <input type="text" name="number" class="form-control" placeholder="Contact">
    <span class="text-danger">{{$errors->first('number')}}</span>
    </div>
      <div class="col">
         <label for="cnic">Cnic</label>
      <input type="text" name="cnic" class="form-control" placeholder="Cnic">
    <span class="text-danger">{{$errors->first('cnic')}}</span>
    </div>
  
 
    <div class="col">
        <label for="cnic">Plot No</label>
      <input type="text" name="plotno" class="form-control" placeholder="Enter Plot Number">
    <span class="text-danger">{{$errors->first('plotno')}}</span>
   
    </div>
    <div class="col">
         <label for="designation">Block</label>
         <select class="form-control" name="block"  >
      <option value="">Choose Block</option>
      <option value="A">A</option>
      <option value="B">B</option>
      <option value="C">C</option>
        <option value="D">D</option>
         </select>
     <span class="text-danger">{{$errors->first('block')}}</span>

    </div>
 

  <button type="submit" class="btn btn-primary mt-3">Submit</button>
</form>
         
        </div>
      </section>
@endsection


@section('scripts')
 <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
@endsection