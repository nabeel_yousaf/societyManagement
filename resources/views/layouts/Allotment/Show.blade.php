@extends('layouts.master')

@section('title')

 Society Dashboard
@endsection


@section('content')
 <div class="container-fluid">
     @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
    @if(session()->get('danger'))
    <div class="alert alert-danger">
      {{ session()->get('danger') }}  
    </div><br />
  @endif
  
          <div class="row mb-2">
            <div class="col-sm-6">
    <a href="/addallotment" class="btn btn-primary">Add Allotment</a>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                <li class="breadcrumb-item active">Show Allotment</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
           <table class="table m-2 table-bordered">
          {{-- <h1 align="center"> Total Data : <span id="total_records"></span></h1> --}}
        <thead class="thead-dark">
       <tr>
          <th >ID</th>
          <th>Name</th>
          <th>Contact</th>
          <th>Cnic</th>
          <th>Plot No</th>

          <th>Block</th>
        
          <th colspan = 2>Actions</th>
        
        </tr>
        </thead>
              <tbody>
     
        @foreach ($allotments as $key => $allotment)
       <tr>
        <td>{{$key + 1}}</td>
        <td>{{$allotment->fullname}}</td>
        <td>{{$allotment->number}}</td>
        <td>{{$allotment->cnic}}</td>
        <td>{{$allotment->plotno}}</td>
        <td>{{$allotment->block}}</td>
          <td>
          <a href="{{ route('allotments.edit', $allotment->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('allotments.destroy', $allotment->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
   
       </tr>
        @endforeach
    
         </tbody>
      
       
    </table>  
    {!! $allotments->links() !!}

  </div>
@endsection




  @section('scripts')
 <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
@endsection