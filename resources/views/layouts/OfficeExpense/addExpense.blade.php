@extends('layouts.master')

@section('title')

 Society Dashboard
@endsection



@section('content')
  <div class="container-fluid">
    
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Add Office Expense</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                <li class="breadcrumb-item active">Add Expense</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
     
  <section class="content">
        <div class="container-fluid">
        <form method="POST" action="{{route('members.store')}}" enctype="multipart/form-data">
    
 <div class="col">
        <label for="name">Employee Salary</label>
      <input type="text" name="employee_salary" class="form-control" placeholder="Employee Salary">
    <span class="text-danger">{{$errors->first('employee_salary')}}</span>
    </div>
    <div class="col mt-2">
        <label for="name">Purchase Goods</label>
      <input type="text" name="goods" class="form-control" placeholder="Goods for sale purchase">
    <span class="text-danger">{{$errors->first('goods')}}</span>
    </div>
                <div class="row">
    <div class="col mt-2">
        <label for="name">QTY</label>
      <input type="text" name="qty" class="form-control" placeholder="qty">
    <span class="text-danger">{{$errors->first('qty')}}</span>
    </div>
    <div class="col mt-2">
         <label for="number">Unit Amount</label>
      <input type="text" name="qty_amount" class="form-control" placeholder="qty amount">
    <span class="text-danger">{{$errors->first('qty_amount')}}</span>
    </div>
  </div>
             <div class="row">
    <div class="col mt-2">
        <label for="name">Maintenance</label>
      <input type="text" name="maintenance" class="form-control" placeholder="Name ">
    <span class="text-danger">{{$errors->first('maintenance')}}</span>
    </div>
    <div class="col mt-2">
         <label for="number">Maintenance Bill</label>
      <input type="text" name="maintenance_amount" class="form-control" placeholder="Amount">
    <span class="text-danger">{{$errors->first('maintenance_amount')}}</span>
    </div>
  </div>
         <div class="row">
    <div class="col mt-2">
        <label for="name">Other Expense</label>
      <input type="text" name="others" class="form-control" placeholder="Others Expense ">
    <span class="text-danger">{{$errors->first('others')}}</span>
    </div>
    <div class="col mt-2">
         <label for="number">Amount</label>
      <input type="text" name="other_amount" class="form-control" placeholder="Amount">
    <span class="text-danger">{{$errors->first('other_amount')}}</span>
    </div>
  </div>
  <div class="row">
      <div class="col mt-2">
        <label for="name">Description</label>
      <textarea type="text" name="description" class="form-control" placeholder="Describe of other expense" rows="3"></textarea>
    <span class="text-danger">{{$errors->first('description')}}</span>
    </div>
     <div class="col mt-5">
        <label for="name">Grand Total</label>
      <input type="text" name="grand_total" class="form-control" placeholder="Grand Total" ></input>
    <span class="text-danger">{{$errors->first('grand_total')}}</span>
    </div>
  </div>
   
  <button type="submit" class="btn btn-primary mt-3">Submit</button>
</form>
         
        </div>
      </section>
@endsection

@section('scripts')
 <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
@endsection