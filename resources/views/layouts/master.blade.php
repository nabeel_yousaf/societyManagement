<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
    @yield('title')

  </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  


  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    
    <!-- Navbar -->
     <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        {{-- <li class="nav-item d-none d-sm-inline-block">
          <a href="/" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="#" class="nav-link">Contact</a>
        </li> --}}
      </ul>

      <!-- SEARCH FORM -->
      <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    John Pierce
                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">I got your message bro</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
             
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
           
            
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>
       
          <li class="nav-item dropdown">
             
            
          <a class="nav-link" id="navbarDropdown" data-toggle="dropdown" href="#">
              {{ Auth::user()->name }} 
                                    <span class="caret"></span> 
                                     <i class="fa fa-angle-down"></i>
                   </a>
          <div class="dropdown-menu dropdown-menu-right">
           <a href="/profile" class="dropdown-item">
              <i class="fas fa-user mr-2"></i>My Profile
              
            </a>
             <a href="/changePassword" class="dropdown-item">
              <i class="fas fa-cog  mr-2"></i>Setting
              
            </a>
           
            <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                      <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                                    </a>
                                  <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                                  @csrf
                                  </form>
                                  </div></li>
                         
        
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
        <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
          style="opacity: .8">
        <span class="brand-text font-weight-light">Society Management</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">{{ Auth::user()->name }} </a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2 slide">
          <ul class="nav nav-pills  nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item ">
              <a href="/admin" class="nav-link active" >
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                  {{-- <i class="right fas fa-angle-left"></i> --}}
                </p>
              </a>
           
            </li>

            <li class="nav-item has-treeview">
              <a href="/members" class="nav-link">
                <i class="nav-icon fas fa-edit"></i>
                <p>
                  Members Management
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/addmembers" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Add Members</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/members" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Show Members</p>
                  </a>
                </li>
              
             
              </ul>
            </li>
               <li class="nav-item has-treeview">
              <a href="/members" class="nav-link">
                <i class="nav-icon fas fa-edit"></i>
                <p>
                  Allotments   
                   <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/addallotment" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Add Allotments</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/allotments" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Show Allotments</p>
                  </a>
                </li>
              
             
              </ul>
            </li>
              <li class="nav-item">
              <a href="pages/widgets.html" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                 View Complains
                  
                </p>
              </a>
            </li>
              <li class="nav-item">
              <a href="pages/widgets.html" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  Plots
                  
                </p>
              </a>
            </li>
              <li class="nav-item has-treeview">
              <a href="/members" class="nav-link">
                <i class="nav-icon fas fa-edit"></i>
                <p>
                  Office Expense
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/addExpense" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Add Expense</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/members" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>View Expense</p>
                  </a>
                </li>
              
             
              </ul>
            </li>
            
               <li class="nav-item">
              <a href="pages/widgets.html" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  Visitors
                  
                </p>
              </a>
            </li>
        
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-edit"></i>
                <p>
                  Billing
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="pages/forms/general.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Electricity Bill</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="pages/forms/advanced.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Security Bill</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="pages/forms/editors.html" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Cleaning Bill</p>
                  </a>
                </li>
             
              </ul>
            </li>
           
      <!-- /.sidebar -->
    </aside>

  
    <div class="content-wrapper">
    
     
      <div class="content-header">
      @yield('content')
  
    </div>
  
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->

  
 @yield('scripts')
  
   <script type="text/javascript">
    $(document).ready(function () {
      // $(".slide ul li a").each(function(){
      //   if($(this).attr("href")=="www.google.com")
      //   $(this).addClass("active");
      // })
        // var url = window.location;
        // $('.slide ul li a[href="'+ url +'"]').parent().addClass('active');
        // $('ul.nav a').filter(function() {
        //      return this.href == url;
        // }).parent().addClass('active');
        //  $('.nav-item a').click(function(){
        //    $('.nav-link').removeClass('active');
        //    $(this).addClass('active');
        //  });
         $('li').click(function(){
          
             $('li').removeClass('active');
               $(this).addClass('active');
             
          
         
         })

    });
</script>
</body>




</html>