@extends('layouts.master')

@section('title')

 Society Dashboard
@endsection


@section('content')
        <div class="container-fluid">
               @if(session()->get('error'))
    <div class="alert alert-danger">
      {{ session()->get('error') }}  
    </div><br />

     @endif
               @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif

<form id="form-change-password" role="form" method="POST" action="{!! route('changePassword') !!}" novalidate class="form-horizontal">
{{ csrf_field() }}
   
  <div class="col mt-3">
        <label for="current_password">Current Password</label>
      <input type="password" id="current_password" name="current_password" class="form-control" placeholder="Current Password">
    </div>
    <div class="col mt-3">
        <label for="new_password">New Password</label>
      <input type="password" id="new_password" name="new_password" class="form-control" placeholder="New Password">
    </div>
    <div class="col mt-3">
        <label for="new_password_confirmation">Confirm Password</label>
      <input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control" placeholder="Confirm Password">
    </div>
  <button type="submit" class="btn btn-danger mt-3">Change Password</button>

</form>
        </div>
@endsection

@section('scripts')
 <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
@endsection