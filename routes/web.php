<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'admin']], function(){
    Route::get('/admin', function () {
    return view('admin.dashboard'); 
});
Route::get('/addmembers', function(){
    return view('Memberview.add');
});
Route::get('/members', function(){
    return view('Memberview.index');
});
Route::get('/addallotment', function(){
    return view('layouts.Allotment.Add');
});
Route::get('/allotments', function(){
    return view('layouts.Allotment.Show');
});
Route::get('/addExpense', function(){
    return view('layouts.OfficeExpense.addExpense');
});
         //Resource Controller
Route::resource('members', 'MemberController');
Route::resource('allotments', 'AllotmentController');
Route::resource('expense', 'OfficeExpenseController');

  // end resource controller

//   Route::get('/profilepicture', 'ProfileController@changePasswordForm');
 
Route::get('/changePassword', 'ProfileController@changePasswordForm');
Route::post('/changepassword', 'ProfileController@updatePassword')->name('changePassword');
 
Route::get('/profile', 'ProfileController@profile')->name('profile');
Route::post('/profile', 'ProfileController@profileUpdate')->name('profileUpdate');

});

 

