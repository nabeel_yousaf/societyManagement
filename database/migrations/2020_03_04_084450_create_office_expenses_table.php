<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficeExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_salary');
            $table->string('goods');
            $table->string('qty');
            $table->string('qty_amount');
            $table->string('maintenance');
            $table->string('maintenance_amount');
            $table->string('others');
            $table->string('description');
            $table->string('other_amount');
            $table->string('grand_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_expenses');
    }
}
